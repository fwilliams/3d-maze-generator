#include <GLTools.h>
#include <GLShaderManager.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include "wall.h"

GLfloat light_pos[] = { 0.0f, 0.0f, -1.5f };
GLfloat light_color[] = { 1.0f, 1.0f, 1.0f, 1.0f };

Wall::Wall(GLuint texture, GLBatch& geometry, GLFrame& frame) {
	this->texture = texture;
	this->geometry = geometry;
	this->frame = frame;
}

void Wall::render(GLShaderManager& shader_man, GLGeometryTransform& pipeline, GLMatrixStack& mv_mat, GLMatrixStack& p_mat) {
	mv_mat.PushMatrix();
	p_mat.PushMatrix();

	M3DMatrix44f mat;
	this->frame.GetMatrix(mat);
	mv_mat.MultMatrix(mat);

	glBindTexture(GL_TEXTURE_2D, this->texture);
	
	shader_man.UseStockShader(
		GLT_SHADER_TEXTURE_POINT_LIGHT_DIFF,
		pipeline.GetModelViewMatrix(),
		pipeline.GetProjectionMatrix(),
		light_pos, light_color, 0);
	
	//shader_man.UseStockShader(GLT_SHADER_TEXTURE_REPLACE, pipeline.GetModelViewProjectionMatrix(), 0);

	this->geometry.Draw();

	mv_mat.PopMatrix();
	p_mat.PopMatrix();
}

void Wall::init_geometry(GLBatch& geometry) {
	geometry.Begin(GL_TRIANGLES, 6, 1);

	geometry.Normal3f(0.0f, 0.0f, -1.0f);
	geometry.MultiTexCoord2f(0, 0.0f, 0.0f);
	geometry.Vertex3f(-0.5f, -0.5f, 0.0f);

	geometry.Normal3f(0.0f, 0.0f, -1.0f);
	geometry.MultiTexCoord2f(0, 1.0f, 0.0f);
	geometry.Vertex3f(0.5f, -0.5f, 0.0f);	

	geometry.Normal3f(0.0f, 0.0f, -1.0f);
	geometry.MultiTexCoord2f(0, 0.0f, 1.0f);
	geometry.Vertex3f(-0.5f, 0.5f, 0.0f);

	geometry.Normal3f(0.0f, 0.0f, -1.0f);
	geometry.MultiTexCoord2f(0, 1.0f, 0.0f);
	geometry.Vertex3f(0.5f, -0.5f, 0.0f);	

	geometry.Normal3f(0.0f, 0.0f, -1.0f);
	geometry.MultiTexCoord2f(0, 1.0f, 1.0f);
	geometry.Vertex3f(0.5f, 0.5f, 0.0f);

	geometry.Normal3f(0.0f, 0.0f, -1.0f);
	geometry.MultiTexCoord2f(0, 0.0f, 1.0f);
	geometry.Vertex3f(-0.5f, 0.5f, 0.0f);

	geometry.End();
}
