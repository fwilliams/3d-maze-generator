#include "cell.h"

Cell::Cell(int x, int y) {
	this->position[0] = x;
	this->position[1] = y;
	for(int i=0; i<4; i++) {
		this->adjacent_cells[i] = 0;
	}
}

CellAdjacency Cell::get_adjacent_direction(Cell &other) {
	if( (this->position[0]-other.position[0]) == 1  && (this->position[1]-other.position[1]) == 0 ) {
		return LEFT;
	}
	if( (this->position[0]-other.position[0]) == -1 && (this->position[1]-other.position[1]) == 0 ) {
		return RIGHT;
	}
	if( (this->position[1]-other.position[1]) == 1 && (this->position[0]-other.position[0]) == 0 ) {
		return UP;
	}
	if( (this->position[1]-other.position[1]) == -1 && (this->position[0]-other.position[0]) == 0 ) {
		return DOWN;
	}
	return NOT_ADJACENT;
}

void Cell::set_union(Cell &other) {
	CellAdjacency adj = this->get_adjacent_direction(other);
	if(adj != NOT_ADJACENT) {
		this->adjacent_cells[adj] = &other;
		other.adjacent_cells[get_opposite_adjacency(adj)] = this;
	} else {
		// explode!
	}
}