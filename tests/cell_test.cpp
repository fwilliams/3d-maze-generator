
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE CellTest
#include <boost/test/unit_test.hpp>

#include "cell.h"

class CellTestFixture {
	public:
		CellTestFixture() {
		}

		~CellTestFixture() {
		}
};

BOOST_FIXTURE_TEST_SUITE(CellTests, CellTestFixture);

BOOST_AUTO_TEST_CASE( test_cell_adjacency_is_correct ) {
	Cell* cell0 = new Cell(0, 0);
	Cell* cell1 = new Cell(1, 0);
	Cell* cell2 = new Cell(0, 1);

	BOOST_CHECK( cell0->get_adjacent_direction(*cell1) == RIGHT );
	BOOST_CHECK( cell1->get_adjacent_direction(*cell0) == LEFT );
	BOOST_CHECK( cell0->get_adjacent_direction(*cell2) == DOWN );
	BOOST_CHECK( cell2->get_adjacent_direction(*cell0) == UP );
	BOOST_CHECK( cell2->get_adjacent_direction(*cell1) == NOT_ADJACENT );
}

BOOST_AUTO_TEST_CASE( test_unioning_two_cells_properly_sets_adjacency ) {
	Cell* cell0 = new Cell(0, 0);
	Cell* cell1 = new Cell(1, 0);
	Cell* cell2 = new Cell(0, 1);

	BOOST_CHECK( cell0->adjacent_cells[RIGHT] == 0 );

	cell0->set_union(*cell1);

	BOOST_CHECK( cell0->adjacent_cells[RIGHT] == cell1 );
	BOOST_CHECK( cell1->adjacent_cells[LEFT] == cell0 );


	BOOST_CHECK( cell0->adjacent_cells[DOWN] == 0 );

	cell0->set_union(*cell2);

	BOOST_CHECK( cell0->adjacent_cells[DOWN] == cell2 );
	BOOST_CHECK( cell2->adjacent_cells[UP] == cell0 );
}

BOOST_AUTO_TEST_SUITE_END();
