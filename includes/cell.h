#include "disjoint_set.h"

enum CellAdjacency { LEFT, RIGHT, UP, DOWN, NOT_ADJACENT };

static CellAdjacency get_opposite_adjacency(CellAdjacency adj) {
	switch(adj) {
		case UP:
			return DOWN;
			break;
		case DOWN:
			return UP;
			break;
		case LEFT:
			return RIGHT;
			break;
		case RIGHT:
			return LEFT;
			break;
		case NOT_ADJACENT:
			return NOT_ADJACENT;
			break;
	}
	// TODO: Explode!
	return NOT_ADJACENT;
}

class Cell : public DisjointSetNode {
	public:
		Cell(int, int);
		void set_union(Cell&);
		CellAdjacency get_adjacent_direction(Cell&);
		int position[2];
		DisjointSetNode* adjacent_cells[4];
};
