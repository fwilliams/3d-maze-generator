class DisjointSetNode {
	public:	
		virtual void set_union(DisjointSetNode);
		int id;
};

class DisjointSet {
	public:
		DisjointSet(DisjointSetNode*, int);
		DisjointSetNode* find(DisjointSetNode&);
		void set_union(DisjointSetNode&, DisjointSetNode&);
		bool are_unioned(DisjointSetNode&, DisjointSetNode&);
		int disjoint_set_count();
		DisjointSetNode* nodes;
	private:
		DisjointSetNode** tree;
		int disjoint_sets;
		int node_count;
};