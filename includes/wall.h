class Wall {
	public: 
		Wall(GLuint, GLBatch&, GLFrame&);
		void render(GLShaderManager&, GLGeometryTransform&, GLMatrixStack&, GLMatrixStack&);
		static void init_geometry(GLBatch&);

	private:
		GLBatch geometry;
		GLuint texture;
		GLFrame frame;
};