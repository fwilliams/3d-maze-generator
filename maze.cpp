#include <GLTools.h>
#include <GLShaderManager.h>
#include <GLFrustum.h>
#include <GLMatrixStack.h>
#include <GLGeometryTransform.h>
#include <StopWatch.h>
#include <GL/glut.h>
#include "tools.h"
#include "wall.h"

#define TUNNEL_LENGTH 20

GLShaderManager shaderManager;
GLFrustum viewFrustum;

GLFrame camera_frame;

GLMatrixStack modelview_matrix;
GLMatrixStack projection_matrix;
GLGeometryTransform transform_pipeline;

GLBatch wall_geometry;
GLuint wall_texture;
GLuint floor_texture;
GLuint ceiling_texture;

Wall* r_walls[TUNNEL_LENGTH];
Wall* l_walls[TUNNEL_LENGTH];
Wall* floors[TUNNEL_LENGTH];
Wall* ceilings[TUNNEL_LENGTH];

void change_size(GLsizei, GLsizei);
void setup_rendering_context(void);
void render_scene(void);
void special_keys(int, int, int);

int main(int argc, char** argv) {
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_STENCIL);

	glutInitWindowSize(800, 600);
	glutCreateWindow("Maze!");

	glutReshapeFunc(change_size);
	glutDisplayFunc(render_scene);
	glutSpecialFunc(special_keys);

	GLenum error = glewInit();
	if(error != GLEW_OK) {
		fprintf(stderr, "GLEW Error: %s\n", glewGetErrorString(error));
		return 1;
	}

	setup_rendering_context();

	glutMainLoop();

	return 0;
}

void special_keys(int key, int x, int y) {
	if(key == GLUT_KEY_UP)
		camera_frame.MoveForward(0.2);
	if(key == GLUT_KEY_DOWN)
		camera_frame.MoveForward(-0.2);
	if(key == GLUT_KEY_LEFT)
		camera_frame.RotateWorld(m3dDegToRad(1.0f), 0.0f, 1.0f, 0.0f);
	if(key == GLUT_KEY_RIGHT)
		camera_frame.RotateWorld(m3dDegToRad(-1.0f), 0.0f, 1.0f, 0.0f);
}

void change_size(GLsizei width, GLsizei height) {
	viewFrustum.SetPerspective(35.0f, float(width) / float(height), 0.1f, 100.0f);
	projection_matrix.LoadMatrix(viewFrustum.GetProjectionMatrix());
	glViewport(0, 0, width, height);
}

void setup_rendering_context(void) {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);

	shaderManager.InitializeStockShaders();
	transform_pipeline.SetMatrixStacks(modelview_matrix, projection_matrix);
	
	glGenTextures(1, &wall_texture);
	glBindTexture(GL_TEXTURE_2D, wall_texture);
	load_tga_texture("textures/brick.tga", GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE);

	glGenTextures(1, &floor_texture);
	glBindTexture(GL_TEXTURE_2D, floor_texture);
	load_tga_texture("textures/floor.tga", GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE);

	glGenTextures(1, &ceiling_texture);
	glBindTexture(GL_TEXTURE_2D, ceiling_texture);
	load_tga_texture("textures/ceiling.tga", GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, GL_CLAMP_TO_EDGE);

	Wall::init_geometry(wall_geometry);

	for(int i=0; i<TUNNEL_LENGTH; i++) {
		GLFrame frame_right;
		frame_right.RotateWorld(m3dDegToRad(-90.0f), 0.0f, 1.0f, 0.0f);
		M3DVector3f wall_right_origin = { 0.5f, 0.0f, (float) -i };
		frame_right.SetOrigin(wall_right_origin);
		r_walls[i] = new Wall(wall_texture, wall_geometry, frame_right);

		GLFrame frame_left;
		frame_left.RotateWorld(m3dDegToRad(90.0f), 0.0f, 1.0f, 0.0f);
		M3DVector3f wall_left_origin = { -0.5f, 0.0f, (float) -i };
		frame_left.SetOrigin(wall_left_origin);
		l_walls[i] = new Wall(wall_texture, wall_geometry, frame_left);

		GLFrame frame_floor;
		frame_floor.RotateWorld(m3dDegToRad(-90.0f), 1.0f, 0.0f, 0.0f);
		M3DVector3f floor_origin = { 0.0f, -0.5f, (float) -i };
		frame_floor.SetOrigin(floor_origin);
		floors[i] = new Wall(floor_texture, wall_geometry, frame_floor);

		GLFrame frame_ceiling;
		frame_ceiling.RotateWorld(m3dDegToRad(90.0f), 1.0f, 0.0f, 0.0f);
		M3DVector3f ceiling_origin = { 0.0f, 0.5f, (float) -i };
		frame_ceiling.SetOrigin(ceiling_origin);
		ceilings[i] = new Wall(ceiling_texture, wall_geometry, frame_ceiling);		
	}

	camera_frame.MoveForward(-1.0f);
}

void render_scene(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	modelview_matrix.PushMatrix();
		M3DMatrix44f camera;
		camera_frame.GetCameraMatrix(camera);
		modelview_matrix.MultMatrix(camera);

		for(int i=0; i<TUNNEL_LENGTH; i++) {
			r_walls[i]->render(shaderManager, transform_pipeline, modelview_matrix, projection_matrix);
			l_walls[i]->render(shaderManager, transform_pipeline, modelview_matrix, projection_matrix);
			floors[i]->render(shaderManager, transform_pipeline, modelview_matrix, projection_matrix);
			ceilings[i]->render(shaderManager, transform_pipeline, modelview_matrix, projection_matrix);	
		}

	modelview_matrix.PopMatrix();
	
	glutSwapBuffers();
	glutPostRedisplay();
}