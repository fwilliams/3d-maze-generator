MAIN = maze
SRCPATH = ./
TEST_PATH = $(SRCPATH)tests/
LIBPATH = $(SRCPATH)libs/
SHAREDPATH = ./GLTools/src/
SHAREDINCPATH = ./GLTools/include/
PROJECTINCDIR = -I$(SRCPATH)includes
LIBDIRS = -L/usr/lib/X11 -L/usr/local/lib
INCDIRS = -I/usr/include -I/usr/local/inclu_de -I/usr/include/GL -I$(SHAREDINCPATH)  -I$(SHAREDINCPATH)GL

CC = g++
CFLAGS = $(COMPILERFLAGS) -g $(INCDIRS)
LIBS = -lX11 -lglut -lGL -lGLU -lm

prog : $(MAIN)
$(MAIN) : $(SRCPATH)$(MAIN).cpp
glew.o    : $(SHAREDPATH)glew.c
GLTools.o    : $(SHAREDPATH)GLTools.cpp
GLBatch.o    : $(SHAREDPATH)GLBatch.cpp
GLTriangleBatch.o    : $(SHAREDPATH)GLTriangleBatch.cpp
GLShaderManager.o    : $(SHAREDPATH)GLShaderManager.cpp
math3d.o    : $(SHAREDPATH)math3d.cpp

cell.o    : $(SRCPATH)cell.cpp

$(MAIN) :
	$(CC) $(CFLAGS) -o $(MAIN) $(LIBDIRS) $(SRCPATH)$(MAIN).cpp $(SHAREDPATH)glew.c $(SHAREDPATH)GLTools.cpp $(SHAREDPATH)GLBatch.cpp $(SHAREDPATH)GLTriangleBatch.cpp $(SHAREDPATH)GLShaderManager.cpp $(SHAREDPATH)math3d.cpp $(SRCPATH)wall.cpp $(SRCPATH)tools.cpp $(LIBS)

test:
	$(CC) -o$(TEST_PATH)runtests $(SRCPATH)cell.cpp $(TEST_PATH)cell_test.cpp $(LIBPATH)lib_disjoint_set.a $(PROJECTINCDIR) -lboost_unit_test_framework

clean:
	rm -f *.o